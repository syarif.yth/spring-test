package com.ujian.backend.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ujian.backend.entity.ResultUmur;
import com.ujian.backend.entity.Umur;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

@Controller
@RestController
public class CalculateAge {
    @RequestMapping(value = "/calculateage/{dateOfBirth}",
            method = RequestMethod.GET,
            produces = "Application/JSON")
    @ResponseBody
    public static String hitung(@PathVariable String dateOfBirth)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate curDate = LocalDate.now();
        LocalDate birth = LocalDate.parse(dateOfBirth, formatter);
        Period usia = Period.between(birth, curDate);
        int tahun = usia.getYears();
        int bulan = usia.getMonths();
        int hari = usia.getDays();

        try {
            Umur umur = new Umur(tahun,bulan,hari);
            ResultUmur result = new ResultUmur(umur);
            ObjectMapper mapper = new ObjectMapper();

            return mapper.writeValueAsString(result);
        } catch (Exception e) {
            throw new Error(e);
        }
    }

}
